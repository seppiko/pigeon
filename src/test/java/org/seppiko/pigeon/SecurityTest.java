/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.seppiko.commons.utils.codec.Base64Util;
import org.seppiko.pigeon.utils.AesUtil;
import org.seppiko.pigeon.utils.PasswordUtil;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Leonard Woo
 */
@Slf4j(topic = "securityTest")
public class SecurityTest {

  @Test
  public void passwordTest() {
    // Generator user
    String username = "admin";
    String password = "admin";
    log.info("INSERT INTO `user`(`username`, `password`) VALUES ('"
        + username + "', '" + new BCryptPasswordEncoder().encode(password) + "');");
  }

  // generator pigeon.yml security salt and iv
  @Test
  public void genConfig() {
    SecureRandom secureRandom = new SecureRandom();
    log.info("Salt: " + Base64Util.encodeToString(secureRandom.generateSeed(32)) );

    byte[] iv = new byte[12];
    secureRandom.nextBytes(iv);
    log.info("IV: " + Base64Util.encodeToString(iv) );
  }

  @Test
  public void aesTest() throws Throwable {
    String text = "addddd";
    byte[] key = Base64Util.decode("s92BA39yXzrQREQ65Js9s1RArO3wJxWCuTZ1zs8oAj0=");
    byte[] iv = Base64Util.decode("5gGONcka3S9whXVl");

    byte[] secret = AesUtil.encode(text.getBytes(StandardCharsets.UTF_8), key, iv);
    log.info("--- " + text);
    log.info(">>> " + Base64Util.encodeToString(secret));
    log.info("<<< " + StandardCharsets.UTF_8.decode( ByteBuffer.wrap( AesUtil.decode(secret, key, iv) ) ) );

    String password = PasswordUtil.encode(text, "admin");
    log.info(">>> " + password);
    log.info("<<< " + PasswordUtil.decode(password, "admin"));

  }
}
