/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon;

import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.seppiko.commons.mail.JakartaMailSender;
import org.seppiko.commons.mail.MimeMessageHelper;
import org.seppiko.pigeon.models.ImapConfigEntity;
import org.seppiko.pigeon.models.MailConfigEntity;
import org.seppiko.pigeon.models.SmtpConfigEntity;
import org.seppiko.pigeon.models.MailEntity;
import org.seppiko.pigeon.utils.JsonUtil;
import org.seppiko.pigeon.utils.MailUtil;

/**
 * Mail test
 *
 * @author Leonard Woo
 */
@Slf4j
public class MailTest {

  @Test
  public void configTest() {
    log.info(JsonUtil.toJson(new MailConfigEntity()) + "");
    log.info(JsonUtil.toJson(new MailEntity()) + "");
    log.info((JsonUtil.fromJson("{}", SmtpConfigEntity.class) == null)+ "");
  }

  @Test
  public void sendTest() {
    try {
      SmtpConfigEntity mailConfig = new SmtpConfigEntity();
      mailConfig.setHost("smtp.matoro.net");
      mailConfig.setPort(994);
      mailConfig.setUsername("support@matoro.net");
      mailConfig.setPassword("U97ANXPQduV9uGw");
      mailConfig.setSmtpAuth(true);
      mailConfig.setSmtpTlsEnable(true);
      mailConfig.setSmtpStartTlsEnable(true);
      mailConfig.setSmtpStartTlsRequired(true);

      JakartaMailSender sender = MailUtil.getMailSender(mailConfig);

      MimeMessageHelper mimeMessage = sender.createMimeMessage();
      mimeMessage.setFrom("Support<support@matoro.net>");
      mimeMessage.setTo("Leo<leo@matoro.net>");
      mimeMessage.setSubject("Seppiko Pigeon HTML Mail Test");
      mimeMessage.setText("<h1>HTML Mail Test</h1>", true);

      sender.send(mimeMessage.getMimeMessage());
    } catch (Exception ex) {
      log.error("", ex);
    }
  }

  @Test
  public void pullTest() {
    ImapConfigEntity mailConfig = new ImapConfigEntity();
    mailConfig.setHost("imap.matoro.net");
    mailConfig.setPort(993);
    mailConfig.setUsername("support@matoro.net");
    mailConfig.setPassword("U97ANXPQduV9uGw");
    mailConfig.setImapAuth(true);
    mailConfig.setImapStartTlsEnable(true);
    mailConfig.setImapStartTlsRequired(true);
    ArrayList<MailEntity> mail = MailUtil.mailReceiver(mailConfig);
    if (mail == null || mail.size() < 1) {
      log.info("Not found any unread mail.");
      return;
    }
    log.info("" + JsonUtil.toJson(mail));
  }

  @Test
  public void logTest() throws Throwable {
    log.info("text");
    log.info(MailUtil.hideAdd("Leo<i@l6d.me>", true, true));
    log.info(MailUtil.hideAdd("Leo<i.am@l6d.me>", true, true));

  }

  @Test
  public void networkTest() throws Exception {

    log.info("DNS Provider: " + JsonUtil.toJson(System.getProperties()));
  }

}
