/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.seppiko.pigeon.models.MailEntity;
import org.seppiko.pigeon.services.LogService;
import org.seppiko.pigeon.services.MailService;
import org.seppiko.pigeon.services.UserService;
import org.seppiko.pigeon.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author Leonard Woo
 */
@Slf4j(topic = "springTest")
@SpringBootTest
public class SpringTest {

  @Autowired
  private LogService logService;

  @Test
  public void logTest() {
    log.info("--- START ---");

    MailEntity mail = new MailEntity();
    mail.setFrom("TEST<test@matoro.net>");
    mail.setTo(new String[]{"test@matoro.net"});
    mail.setSubject("Jakarta Mail Test");
    mail.setText("mail test");

    logService.mailLogger(mail);
  }

  @Autowired
  private MailService mailService;

  @Test
  public void mailTest() {
    log.info("--- IMAP ---");
    mailService.queryImapConfigList("admin", "admin").forEach(e -> {
      log.info(e.toString());
    });
    log.info("--- SMTP ---");
    mailService.querySmtpConfigList("admin", "admin").forEach(e -> {
      log.info(e.toString());
    });
  }
}
