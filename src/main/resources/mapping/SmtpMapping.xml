<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2021 the original author or authors.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.seppiko.pigeon.mapper.SmtpMapper">

  <cache eviction="LRU" flushInterval="60000" size="512" readOnly="true"/>

  <resultMap id="result" type="org.seppiko.pigeon.models.SmtpConfigEntity">
    <result column="id" jdbcType="INTEGER" property="id" />
    <result column="uid" jdbcType="INTEGER" property="uid" />
    <result column="host" jdbcType="VARCHAR" property="host" />
    <result column="port" jdbcType="INTEGER" property="port" />
    <result column="username" jdbcType="VARCHAR" property="username" />
    <result column="password" jdbcType="VARCHAR" property="password" />
    <result column="mailFrom" jdbcType="VARCHAR" property="mailFrom" />
    <result column="smtpAuth" jdbcType="BOOLEAN" property="smtpAuth" />
    <result column="smtpTlsEnable" jdbcType="BOOLEAN" property="smtpTlsEnable" />
    <result column="smtpStartTlsEnable" jdbcType="BOOLEAN" property="smtpStartTlsEnable" />
    <result column="smtpStartTlsRequired" jdbcType="BOOLEAN" property="smtpStartTlsRequired" />
  </resultMap>

  <sql id="base_table">
    smtp_config
  </sql>

  <select id="queryAllByUser" resultMap="result" parameterType="int">
    SELECT id, uid, host, port, username, password, mailFrom, smtpAuth, smtpTlsEnable, smtpStartTlsEnable, smtpStartTlsRequired
    FROM <include refid="base_table" />
    WHERE uid = #{uid}
    AND enable = 1
  </select>

  <select id="query" resultType="org.seppiko.pigeon.models.SmtpConfigEntity">
    SELECT id, uid, host, port, username, password, mailFrom, smtpAuth, smtpTlsEnable, smtpStartTlsEnable, smtpStartTlsRequired
    FROM <include refid="base_table" />
    WHERE id = #{id}
    AND uid = #{uid}
    AND enable = 1
  </select>

  <select id="queryAll" resultMap="result" parameterType="int">
    SELECT id, uid, host, port, username, mailFrom, smtpAuth, smtpTlsEnable, smtpStartTlsEnable, smtpStartTlsRequired
    FROM <include refid="base_table" />
    WHERE enable = 1
  </select>

  <insert id="add" useGeneratedKeys="true" flushCache="true"
    parameterType="org.seppiko.pigeon.models.SmtpConfigEntity"
    keyColumn="config.id" keyProperty="id">
    INSERT INTO <include refid="base_table" />
    (uid, host, port, username, password, mailFrom, smtpAuth, smtpTlsEnable, smtpStartTlsEnable, smtpStartTlsRequired)
    VALUES
    (#{config.uid}, #{config.host}, #{config.port}, #{config.username}, #{config.password},
      #{config.mailFrom}, #{config.smtpAuth}, #{config.smtpTlsEnable}, #{config.smtpStartTlsEnable},
      #{config.smtpStartTlsRequired})
  </insert>

  <update id="update" flushCache="true"
    parameterType="org.seppiko.pigeon.models.SmtpConfigEntity">
    UPDATE <include refid="base_table" />
    <set>
      <if test="config.host != null">
        host = #{config.host},
      </if>
      <if test="config.port != null">
        port = #{config.port},
      </if>
      <if test="config.username != null">
        username = #{config.username},
      </if>
      <if test="config.password != null">
        password = #{config.password},
      </if>
      <if test="config.mailFrom != null">
        mailFrom = #{config.mailFrom},
      </if>
      <if test="config.smtpAuth != null">
        smtpAuth = #{config.smtpAuth},
      </if>
      <if test="config.smtpTlsEnable != null">
        smtpAuth = #{config.smtpTlsEnable},
      </if>
      <if test="config.smtpStartTlsEnable != null">
        smtpStartTlsEnable = #{config.smtpStartTlsEnable},
      </if>
      <if test="config.smtpStartTlsRequired != null">
        smtpStartTlsRequired = #{config.smtpStartTlsRequired}
      </if>
    </set>
    WHERE id = #{config.id}
    AND uid = #{config.uid}
    AND enable = 1
  </update>

  <update id="delete" flushCache="true" parameterType="int">
    UPDATE <include refid="base_table" />
    SET enable = 0
    WHERE id = #{id}
    AND uid = #{uid}
  </update>

</mapper>
