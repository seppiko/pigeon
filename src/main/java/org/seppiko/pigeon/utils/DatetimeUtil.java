/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.utils;

import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Leonard Woo
 */
public class DatetimeUtil {

  // 2006-01-02T15:04:05.0000000Z-07:00
  private static final String ZONE_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'xxxx";

  // UTC
  public static String now() {
    return now(Clock.systemUTC().getZone(), DateTimeFormatter.ofPattern(ZONE_DATETIME_FORMAT));
  }

  public static String now(ZoneId zoneId, DateTimeFormatter formatter) {
    return ZonedDateTime.now(zoneId).format(formatter);
  }
}
