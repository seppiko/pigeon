/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.utils;

import org.seppiko.pigeon.models.ControllerResponseEntity;
import org.seppiko.pigeon.models.ResponseMessageEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * @author Leonard Woo
 */
public class ResponseUtil {

  public static <T> ControllerResponseEntity generatorResponse (int status, T t) {
    return generatorResponse(status, JsonUtil.toJson(t));
  }

  public static ControllerResponseEntity generatorResponse (int status, String json) {
    return new ControllerResponseEntity(status, MediaType.APPLICATION_JSON, json.getBytes());
  }

  public static ResponseEntity<byte[]> sendResponse(ControllerResponseEntity resp) {
    return sendResponse(resp.getStatus(), resp.getMediaType(), resp.getData());
  }

  public static ResponseEntity<byte[]> sendResponse(int status, MediaType type, byte[] content) {
    return ResponseEntity.status(status).contentType(type).contentLength(content.length).body(content);
  }
}
