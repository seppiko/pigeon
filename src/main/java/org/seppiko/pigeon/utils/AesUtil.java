/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.seppiko.commons.utils.crypto.CryptoUtil;
import org.seppiko.commons.utils.crypto.spec.KeySpecUtil;
import org.seppiko.commons.utils.crypto.spec.ParamterSpacUtil;
import org.seppiko.pigeon.exceptions.PigeonAESException;

/**
 * AES with AEAD
 *
 * @author Leonard Woo
 */
public class AesUtil {

  private static final String CIPHER_ALGORITHM = "AES_256/GCM/NoPadding";
  private static final String KEY_ALGORITHM = "AES";
  public static final int GCM_TAG_LENGTH = 16;

  public static byte[] encode(byte[] src, byte[] key, byte[] iv) throws PigeonAESException {
    try{
      GCMParameterSpec gcmParameterSpec = ParamterSpacUtil.getGCM(GCM_TAG_LENGTH * 8, iv);
      SecretKeySpec keySpec = KeySpecUtil.getSecret(key, KEY_ALGORITHM);
      return CryptoUtil.cipher(CIPHER_ALGORITHM, Security.getProvider("Conscrypt"), Cipher.ENCRYPT_MODE, keySpec,
          gcmParameterSpec, src);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
        InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
      throw new PigeonAESException("", ex);
    }
  }

  public static byte[] decode(byte[] src, byte[] key, byte[] iv) throws PigeonAESException {
    try {
      GCMParameterSpec gcmParameterSpec = ParamterSpacUtil.getGCM(GCM_TAG_LENGTH * 8, iv);
      SecretKeySpec keySpec = KeySpecUtil.getSecret(key, KEY_ALGORITHM);
      return CryptoUtil.cipher(CIPHER_ALGORITHM, Security.getProvider("Conscrypt"), Cipher.DECRYPT_MODE, keySpec,
          gcmParameterSpec, src);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException |
        InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException ex) {
      throw new PigeonAESException("", ex);
    }
  }
}
