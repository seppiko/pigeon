/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.seppiko.commons.utils.codec.Base64Util;
import org.seppiko.pigeon.exceptions.PigeonException;
import org.seppiko.pigeon.models.ControllerResponseEntity;
import org.seppiko.pigeon.models.MailEntity;
import org.seppiko.pigeon.models.ResponseMessageEntity;
import org.seppiko.pigeon.services.MailService;
import org.seppiko.pigeon.utils.DatetimeUtil;
import org.seppiko.pigeon.utils.JsonUtil;
import org.seppiko.pigeon.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leonard Woo
 */
@RestController
@RequestMapping(value = "/mail")
public class MailController {

  @Autowired
  private MailService mailService;

  @GetMapping(value = "/{id}")
  public ResponseEntity<byte[]> mailContentHandleExecution (
      @RequestHeader(required = false) Map<String, String> headers,
      @PathVariable(name = "id", required = false) Integer id) {
    ControllerResponseEntity resp;

    if (headers == null || !headers.containsKey("Authorization")) {
      resp = ResponseUtil.generatorResponse(401,
          new ResponseMessageEntity(401, "Failed to obtain authorization"));
    } else {
      String authorization = headers.get("Authorization").split(" ")[1];
      String[] account = new String(Base64Util.decode(authorization)).split(":");

      List<MailEntity> mails = mailService.receivers(id, account[0], account[1]);
      if ( mails != null && mails.size() > 0 ) {
        // success
        resp = ResponseUtil.generatorResponse(200, JsonUtil.toJson(mails));
      } else {
        // failure
        resp = ResponseUtil.generatorResponse(200,
            new ResponseMessageEntity(404, "Not found any unread mail"));
      }
    }
    return ResponseUtil.sendResponse(resp);
  }

  @PostMapping(value = "/{id}")
  public ResponseEntity<byte[]> mailContentHandleExecution (
      @RequestHeader(required = false) Map<String, String> headers,
      @PathVariable(name = "id", required = false) Integer id,
      @RequestBody(required = false) String requestBody) {
    if (requestBody == null) {
      requestBody = "{}";
    }
    MailEntity mail = JsonUtil.fromJson(requestBody, MailEntity.class);
    ControllerResponseEntity resp;
    if (mail == null || id == null) {
      // Not found
      resp = ResponseUtil.generatorResponse(404,
          new ResponseMessageEntity(404, "Could not find a valid mail"));
    } else if (headers == null || !headers.containsKey("Authorization")) {
      resp = ResponseUtil.generatorResponse(401,
          new ResponseMessageEntity(401, "Failed to obtain authorization"));
    } else {
      String authorization = headers.get("Authorization").split(" ")[1];
      String[] account = new String(Base64Util.decode(authorization)).split(":");
      try {
        if ( mailService.sender(mail, id, account[0], account[1]) ) {
          // success
          resp = ResponseUtil.generatorResponse(200,
                  new ResponseMessageEntity(200, "Sent successfully"));
        } else {
          // failure
          resp = ResponseUtil.generatorResponse(200,
                  new ResponseMessageEntity(400, "Failed to send"));
        }
      } catch (PigeonException ex) {
        resp = ResponseUtil.generatorResponse(200,
            new ResponseMessageEntity(400, ex.getMessage()));
      }
    }
    return ResponseUtil.sendResponse(resp);
  }

  @GetMapping(value = "/queryall")
  public ResponseEntity<byte[]> queryAllContentHandleExecution (
      @RequestHeader(required = false) Map<String, String> headers) {

    if (headers == null || !headers.containsKey("Authorization")) {
      return ResponseUtil.sendResponse(
          ResponseUtil.generatorResponse(401,
              new ResponseMessageEntity(401, "Failed to obtain authorization")));
    }

    String authorization = headers.get("Authorization").split(" ")[1];
    String[] account = new String(Base64Util.decode(authorization)).split(":");

    LinkedHashMap<String, List<?>> config = mailService.queryAllConfigMap(account[0]);
    if (config == null) {
      return ResponseUtil.sendResponse(
          ResponseUtil.generatorResponse(404,
              new ResponseMessageEntity(404,
                  "No SMTP/IMAP configuration was found or the user is not an administrator")));
    }

    ObjectNode json = JsonUtil.createJsonObject();
    json.put("status", 200);
    json.put("message", "success");
    json.put("timestamp", DatetimeUtil.now());
    json.putPOJO("smtp", config.get("smtp"));
    json.putPOJO("imap", config.get("imap"));

    return ResponseUtil.sendResponse( ResponseUtil.generatorResponse(200, json) );
  }
}
