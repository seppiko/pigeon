/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.HashMap;
import org.seppiko.pigeon.models.ResponseMessageEntity;
import org.seppiko.pigeon.models.UserEntity;
import org.seppiko.pigeon.services.MailService;
import org.seppiko.pigeon.services.UserService;
import org.seppiko.pigeon.utils.JsonUtil;
import org.seppiko.pigeon.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leonard Woo
 */
@RestController
@RequestMapping(value = "/user")
public class AccountController {

  @Autowired
  private UserService userService;

  @Autowired
  private MailService mailService;

  @GetMapping(value = "/info")
  public ResponseEntity<byte[]> infoPasswordContentHandleExecution () {

    String username = "";
    UserEntity user = userService.queryUser(username);

    HashMap<String, Object> dataMap = new HashMap<>();
    dataMap.put("roles", "");
    dataMap.put("name", user.getNickname());

    return ResponseUtil.sendResponse(ResponseUtil.generatorResponse(200,
        new ResponseMessageEntity(200, "", dataMap)));
  }

  @PostMapping(value = "/change")
  public ResponseEntity<byte[]> changePasswordContentHandleExecution (
      @RequestBody(required = false) String requestBody) {

    if (!StringUtils.hasText(requestBody) || "{}".equals(requestBody)) {

      return ResponseUtil.sendResponse(ResponseUtil.generatorResponse(404,
          new ResponseMessageEntity(404, "Could not find a valid user")));
    } else {
      JsonNode root = JsonUtil.fromJsonNode(requestBody);
      if (root == null) {
        return ResponseUtil.sendResponse(ResponseUtil.generatorResponse(400,
            new ResponseMessageEntity(400, "Json parse failed")));
      } else {
        String username = root.get("username").asText();
        String oldPassword = root.get("password").asText();
        String newPassword = root.get("newPassword").asText();
        if ( userService.changePassword(username, oldPassword, newPassword) ) {
          mailService.querySmtpConfigList(username, oldPassword).forEach(mailConfig -> {
            mailService.updateSmtpConfig(mailConfig, username, newPassword);
          });
          mailService.queryImapConfigList(username, oldPassword).forEach(mailConfig -> {
            mailService.updateImapConfig(mailConfig, username, newPassword);
          });
          return ResponseUtil.sendResponse(ResponseUtil.generatorResponse(200,
              new ResponseMessageEntity(200, "Password change successfully")));
        }
      }
    }
    return ResponseUtil.sendResponse(ResponseUtil.generatorResponse(200,
        new ResponseMessageEntity(400, "Failed to password change")));
  }
}