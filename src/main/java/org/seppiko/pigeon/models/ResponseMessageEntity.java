/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.pigeon.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.seppiko.commons.utils.ObjectUtil;

/**
 * @author Leonard Woo
 */
@AllArgsConstructor
@Getter
public class ResponseMessageEntity {

  private int status;

  private String message;

  private Object data;

  public ResponseMessageEntity(int status, String message) {
    this.status = status;
    this.message = message;
    this.data = null;
  }
}
