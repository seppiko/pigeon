# Seppiko Pigeon

## Introduction

Seppiko Pigeon is a mail security getaway that supports BASIC authorization and authentication, 
and use AES to protect your email account password.

## Configuration

You need `pigeon.yml` OR `pigeon.yaml` in `pigeon.jar` folder
```
-Dfile.encoding=UTF-8 -Dspy.properties=./spy.properties
```

OR:
```
-Dpigeon.configFile=./pigeon.yml
-Dlog4j.configurationFile=./log4j2.xml
-Duser.timezone=UTC
```

## How to start

1. Generator salt and iv:
```java
SecureRandom secureRandom = new SecureRandom();
System.out.println("Salt: " + Base64Util.encodeToString(secureRandom.generateSeed(32)) );
byte[] iv = new byte[12];
secureRandom.nextBytes(iv);
System.out.println("IV: " + Base64Util.encodeToString(iv) );
```
> See `src/test/java/` in `org.seppiko.pigeon.SecurityTest.genConfig`

2. Add Salt and IV in `pigeon.yml` and config

3. Create Database and import `init.sql`
> See Database

3. run this project

## API Document

See [Seppiko Pigeon Document][spdoc]

[spdoc]: https://seppiko.org/pigeon/doc/index.html

## Database

See `init.sql`

Add default account:

```java
String username = "";
String password = "";
System.out.println("INSERT INTO `user`(`username`, `password`) VALUES ('" 
   + username + "', '" + new BCryptPasswordEncoder().encode(password) + "');");
```

Run this SQL

See `src/test/java/` in `org.seppiko.pigeon.SecurityTest.passwordTest`

## Work with Docker

See [https://spring.io/guides/gs/spring-boot-docker/](https://spring.io/guides/gs/spring-boot-docker/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would 
like to change.

Please make sure to update tests as appropriate

## License
This project is released under [Apache License,Version 2.0][alv2].

[alv2]: https://www.apache.org/licenses/LICENSE-2.0

## Sponsors

<a href="https://www.jetbrains.com/" target="_blank"><img src="https://seppiko.org/images/jetbrains.png" alt="JetBrians" width="100px"></a>
