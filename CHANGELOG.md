# Seppiko Pigeon

## 1.7.RELEASE - 2021
1. Update copyright
2. Update Spring boot 2.5.5
3. Change login `BASIC` to `TOKEN`
4. Update Seppiko Commons Utils 1.5.0.RELEASE

## 1.6.RELEASE - 2020-12-16
1. Change `hideAdd` is number, between 0 and 3
2. Fixed bug

## 1.5.RELEASE - 2020-12-14
1. Fixed bug
2. Add `mailSpy` log switch
3. Lombok is back
4. Add `onlyAdd` and `hideAdd` for **Privacy Policy**
5. Add [Google Conscrypt][conscrypt]
6. Add role manager

```sql
CREATE TABLE `role` (
`rid` SMALLINT(5) UNSIGNED NOT NULL,
`name` VARCHAR(10) NOT NULL
)
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB;

INSERT INTO `role` (`rid`, `name`) VALUES ('1', 'ADMIN');
INSERT INTO `role` (`rid`, `name`) VALUES ('2', 'USER');

CREATE TABLE `user_role` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
`uid` INT(10) UNSIGNED NOT NULL,
`rid` SMALLINT(5) UNSIGNED NOT NULL,
PRIMARY KEY (`id`)
)
DEFAULT CHARSET=utf8mb4
ENGINE=InnoDB;

```

[conscrypt]: https://github.com/google/conscrypt

## 1.4.RELEASE - 2020-11-30
1. Add data checker
2. Set cache ttl is 10
3. Change mail `port` length is 5
4. Update document
5. Delete Lombok
6. Update Seppiko Commons Mail to 1.0.2.RELEASE
7. Update Seppiko Commons Utils to 1.0.3.RELEASE
8. Add `smtpTlsEnable` and `imapTlsEnable`

```sql
ALTER TABLE `smtp_config`
ADD COLUMN `smtpTlsEnable` TINYINT(1) NOT NULL AFTER `smtpAuth`;
CHANGE COLUMN `port` `port` INT(5) UNSIGNED NOT NULL AFTER `host`;
ALTER TABLE `imap_config`
ADD COLUMN `imapTlsEnable` TINYINT(1) NOT NULL AFTER `imapAuth`;
CHANGE COLUMN `port` `port` INT(5) UNSIGNED NOT NULL AFTER `host`;
```

## 1.3.RELEASE - 2020-11-15
1. Update Spring Boot to 2.4.0
2. Change spring-boot-starter-mail to Seppiko Commons Mail
3. Change `imapTls` and `imapFallback` to `imapStartTlsEnable` and `imapStartTlsRequired`
4. Add Seppiko Commons Util
5. Add PostgreSQL client
6. Remove `spy.properties` `driverlist`

```sql
ALTER TABLE `imap_config`
CHANGE COLUMN `imapTls` `imapStartTlsEnable` TINYINT(1) NOT NULL AFTER `imapAuth`,
CHANGE COLUMN `imapFallback` `imapStartTlsRequired` TINYINT(1) NOT NULL AFTER `imapStartTlsEnable`;
```

## 1.2.RELEASE - 2020-10-27
1. Add change password
2. Add IMAP support (Base on Jakarta Mail)
3. Split SMTP controller from MailController
4. Add proxy support
5. fixed bug

## 1.1.RELEASE - 2020-10-25
1. Add CRUD by SMTP config
2. Change JSON not null
3. Add SMTP configuration ID for send mail
4. Unbind user mail, support one-to-many SMTP configuration
5. Fix bug

## 1.0.RELEASE - 2020-10-23
1. Implement Spring Mail
2. Support send mail with POST
3. Implement Spring Security Basic Authentication
